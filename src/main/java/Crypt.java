public class Crypt {

    /**
     * Decrypts a Caesar cipher on a string
     *
     * @param message the encrypted text
     * @param key     the shift amount
     * @return the plaintext
     */
    public static String decrypt(String message, int key) {
        return encrypt(message, -key);
    }

    /**
     * Puts a simple Caesar cipher on a string, not secure at all
     *
     * @param message the plaintext
     * @param key     the shift amount
     * @return the encrypted text
     */
    public static String encrypt(String message, int key) {
        char[] chars = message.toCharArray();
        for (int i = 0; i < message.length(); i++) {
            char c = chars[i];
            if (c >= 32 && c <= 127) {
                // Change base to make life easier, and use an
                // int explicitly to avoid worrying... cast later
                int x = c - 32;
                x = (x + key) % 96;
                if (x < 0) {
                    x += 96; // java modulo can lead to negative values!
                }
                chars[i] = (char) (x + 32);
            }
        }
        return new String(chars);
    }
}
